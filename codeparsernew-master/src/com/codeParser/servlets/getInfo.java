package com.codeParser.servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.Statement;

import javax.servlet.GenericServlet;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebServlet;


@WebServlet("/getInfo")
public class getInfo extends GenericServlet {
	private Connection con;
	private Statement stmt;
	
	public void service(ServletRequest request, ServletResponse response) throws ServletException, IOException {
		response.setContentType("text/html");
		PrintWriter out=response.getWriter();
		String databaseType=request.getParameter("database");
		System.out.println(databaseType);
		if(!databaseType.equals("sql")) {
			out.write("Did not add support for the oracle database ...");
			out.close();
		}
		else {
			
			String userName=request.getParameter("Name");
			String password=request.getParameter("Password");
			String ipAdd=request.getParameter("IPAdd");
			String dbName=request.getParameter("DBName");
			String queryString=String.format("jdbc:mysql://%s/%s",ipAdd,dbName);
			System.out.println(queryString);
			try {
				Class.forName("com.mysql.jdbc.Driver");
				con=DriverManager.getConnection(queryString.toString(),userName,password);
				//out.write("Connection successful");
				RequestDispatcher requestDispatcher = request.getRequestDispatcher("queryServlet");
				request.setAttribute("message","Welcome");
				requestDispatcher.forward(request, response);
			}
			catch(Exception e) {
				RequestDispatcher requestDispatcher = request.getRequestDispatcher("index.jsp");
				requestDispatcher.include(request, response);
				//e.printStackTrace();
			}
			
		
		}
		
	}

}
